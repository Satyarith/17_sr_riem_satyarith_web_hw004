import './App.css';
import Compon from './Components/Compon';
import Table from './Components/Table';

import FormComponent from './Components/FormComponent';
import TableComponent from './Components/TableComponent';

function App() {
  return (
    <div className="App bg-green-300 h-screen">
      {/* <TableComponent />
      <FormComponent/> */}
      
      <div class="text-green-600 text-5xl">Please fill your Information</div>
      
      <Compon />
      
      {/* <div class="m-8">
        <Table/>
      </div> */}
    </div>
  
  );
}

export default App;
