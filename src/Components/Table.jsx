import React, { Component } from "react";
import Swal from "sweetalert2";  

export default class Table extends Component {

  showInfo = () => {
    this.props.student.map(item => (
          Swal.fire({
  html:`id: ${item.id} <br> email: ${item.e} <br>Username: ${item.username} <br>age: ${item.age}`,
   
  // showCloseButton: true,
  // focusConfirm: false,
  confirmButtonText:
    '<i class="fa fa-thumbs-up"></i> ok',
  
})
    ))

  }



  render() {
    return (
      <div class="bg-sky-400">
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
          <table class="w-full text-sm text-left text-white dark:text-gray-400">
            <thead class="text-xs text-blue-600 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
              <tr>
                <th scope="col" class="px-6 py-3">
                  ID
                </th>
                <th scope="col" class="px-6 py-3">
                  EMAIL
                </th>
                <th scope="col" class="px-6 py-3">
                  USERNAME
                </th>
                <th scope="col" class="px-6 py-3">
                  AGE
                </th>
                <th scope="col" class="px-20 py-4">
                  <div> Action </div>
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.student.map((item) => (
                <tr class="odd:bg-blue-500">
                  <td>{item.id}</td>
                  <td>{item.e}</td>
                  <td>{item.username}</td>
                  <td>{item.age}</td>
                      <td>
                          <button onClick={()=>this.props.handleStatus(item.id)} class="bg-red-500 hover:bg-red-700 text-white font-bold py-3 px-8 rounded-full mx-2">
                            {item.status}
                          </button>
                         <button onClick={this.showInfo} class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-3 px-6 rounded-full mx-2">
                            Show more
                          </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
