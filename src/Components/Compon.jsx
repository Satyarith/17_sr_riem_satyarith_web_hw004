import React, { Component } from "react";
import Table from "./Table";
import { MdOutgoingMail } from "react-icons/md";
import { MdOutlineTextRotationAngledown } from "react-icons/md";
import { MdOutlineAccountBox } from "react-icons/md";

export default class Compon extends Component {
  constructor() {
    super();
    this.state = {
      StudentName: [
        {
          id: 1,
          e: "riemSatyarithboysmos@gamil.com",
          username: "Satya Rith",
          age: 18,
          status: "Pending",
        },
      ],
      newEmail: "",
      newUsername: "",
      newAge: "",
      newStatus: "Pending",
    };
  }

  handleStatus = (v) => {
    this.state.StudentName.map(st => {
      if (st.id == v) {
        if (st.status == "Pending") {
          st.status = "done"
        }
        else {
          st.status= "Pending"
        }
      }
    })
    this.setState({
      st: this.state.StudentName
    })
  }

  handleEmailInput = (v) => {
    this.setState({
      newEmail: v.target.value,
    });
  };
  handleNameInput = (v) => {
    this.setState({
      newUsername: v.target.value,
    });
  };
  handleAgeInput = (v) => {
    this.setState({
      newAge: v.target.value,
    });
  };
  onSubmit = () => {
    // console.log("Hello")
    const newObject = {
      id: this.state.StudentName.length + 1,
      e: this.state.newEmail,
      username: this.state.newUsername,
      age: this.state.newAge,
      status: "Pending",
    };

    console.log(newObject);

    this.setState(
      {
        StudentName: [...this.state.StudentName, newObject],
        newEmail: "",
        newUsername: "",
        newAge: "",
      },
      () => console.log(this.state.StudentName)
    );
  };

  render() {
    return (
      <div class="container mx-auto w-[50%]">
        <div>
          {/* Email */}
          <div >
            <label
              class="block text-sky-600 text-sm font-bold mb-2 text-2xl text-left"
              for="name@gmail.com"
            >
              Your Email
            </label>
            <div className="relative">
              <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <MdOutgoingMail />
              </div>
              <input
                onChange={this.handleEmailInput}
                class="pl-10 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                id="name@gail.com"
                type="text"
                placeholder="name@gmail.com"
                value={this.state.newEmail}
              />
            </div>
          </div>
        

        {/* User Name */}
        <div>
          <label
              class="block text-sky-600 text-sm font-bold mb-2 text-2xl text-left"
              for="username"
            >
              User Name
          </label>
            <div className="relative">
              <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <MdOutlineTextRotationAngledown />
            </div>
              <input
                onChange={this.handleNameInput}
                class="pl-10 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                id="username"
                type="text"
              placeholder="Username"
              value={this.state.newUsername}
              />
            </div>
        </div>
      

        {/* Age */}
        <div>
          <label
              class="block text-sky-600 text-sm font-bold mb-2 text-2xl text-left"
              for="Age"
            >
              Age
            </label>
            
            <div className="relative">
              <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <MdOutlineAccountBox />
            </div>
              <input
                onChange={this.handleAgeInput}
                class="pl-10 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                id="Age"
                type="number"
              placeholder="Username"
              value={this.state.newAge}
              />
            </div>
        </div>
      
        {/* Button */}
        <div class="text-center">
          <button
            onClick={this.onSubmit}
            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-10 rounded-full m-4"
          >
            Submit
          </button>
        </div>

        <Table student={this.state.StudentName} handleStatus={ this.handleStatus} />
        </div>
        </div>
    );
  }
}
